RTVS Search Engine README
=============

RTVS Search Engine enables people to search the original video file from specific video clip.
1. input: a video clip path through socket communication.
2. RTVS Search Engine instantly get the fingerprints of the video clip.
3. RTVS Search Engine searches the most similar videos from the fingerprints.
4. output: series of video files.
 
## Libraries


## Tools


## Documentation

The offline documentation is available in the **doc/** directory.

### Examples

Coding examples are available in the **doc/examples** directory.

## License

RTVS Fingerprint Extractor is mainly LGPL-licensed with optional components licensed under
GPL. Please refer to the LICENSE file for detailed information.