﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using MAMVideoSearch.Models;
using BjSTools.MultiMedia;

namespace MAMVideoSearch
{
    class TCPServer
    {
        private TcpListener tcpListener;
        private Thread listenThread;

        public TCPServer()
        {
          this.tcpListener = new TcpListener(IPAddress.Any, 7788);
          this.listenThread = new Thread(new ThreadStart(ListenForClients));
          this.listenThread.Start();
        }   

        private void ListenForClients()
        {
          this.tcpListener.Start();

          while (true)
          {
            //blocks until a client has connected to the server
            TcpClient client = this.tcpListener.AcceptTcpClient();

            //create a thread to handle communication 
            //with connected client
            Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
            clientThread.Start(client);
          }
        }

        private void HandleClientComm(object client)
        {
            TcpClient tcpClient = (TcpClient)client;
            NetworkStream clientStream = tcpClient.GetStream();

            string results = "";
            string fileName = "";
            string strResults = "";
            int fileLength = 0;
            byte[] message = new byte[4096];

            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] buffer = null;
            int bytesRead;
            try
            {
                while (true)
                {
                    bytesRead = 0;

                    try
                    {
                        //blocks until a client sends a message
                        bytesRead = clientStream.Read(message, 0, 4096);
                    }
                    catch
                    {
                        //a socket error has occured
                        break;
                    }

                    if (bytesRead == 0)
                    {
                        //the client has disconnected from the server
                        break;
                    }

                    //message has successfully been received
                    Console.WriteLine(DateTime.Now + ": " + encoder.GetString(message, 0, bytesRead));
                    System.Diagnostics.Debug.WriteLine(encoder.GetString(message, 0, bytesRead));
                    results += encoder.GetString(message, 0, bytesRead);

                    if(!string.IsNullOrEmpty(results)) 
                    {
                        while((fileLength = results.IndexOf("\r\n"))!=-1)
                        {
                            fileName = results.Substring(0, fileLength);
                            results = results.Substring(fileLength + 2, results.Length - fileLength - 2);
                            Debug.WriteLine("{0} fileName: {1}", DateTime.Now, fileName);
                            Debug.WriteLine("{0} results: {1} ", DateTime.Now, results);
                            Debug.Flush();

                            strResults = getDNAfromFile(fileName);

                            // 업로드된 파일에 대해서 처리하고, 회신함.
                            buffer = encoder.GetBytes(strResults + (strResults.Substring(strResults.Length-2,2).IndexOf("\r\n")!=-1?"\r\n":"\r\n\r\n"));
                            clientStream.Write(buffer, 0, buffer.Length);
                            clientStream.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Exception: " + ex.Message);
            }

            tcpClient.Close();
        }

        private string getDNAfromFile(string fileName)
        {
            List<DNA> dnas = new List<DNA>();

            FFmpegMediaInfo vInfo = new FFmpegMediaInfo(fileName);
            //vInfo.GetSnapshot((fileName.Replace("http://localhost:8088", @"c:\Resources")).Replace("/", @"\"), 10 * 5, "C:\\Resources\\VideosUploaded\\snapshots\\ss");
            vInfo.GetSnapshot(fileName, 10 * 5, "C:\\RTVS\\Resources\\VideosUploaded\\snapshots\\ss");

            float r11, r12, r21, r22;
            float g11, g12, g21, g22;
            float b11, b12, b21, b22;
            long height = 0;
            long width = 0;
            float hw = 0;

            byte[] fileData;
            Bitmap bmp;
            int num_frames = 1;
            string strFrameNumber = num_frames.ToString();
            string filename = "c:\\RTVS\\Resources\\VideosUploaded\\snapshots\\ss-"
                + "00".Substring(0, 2 - strFrameNumber.Length) + strFrameNumber
                + ".jpg";

            //Dictionary<TimeSpan, Bitmap> snapshots = new Dictionary<TimeSpan, Bitmap>();
            while (File.Exists(filename))
            {
                Debug.WriteLine("{0}: {1}", DateTime.Now, num_frames);
                fileData = File.ReadAllBytes(filename);
                bmp = new Bitmap(new MemoryStream(fileData));
                //File.Delete(filename);

                if (bmp != null)
                {
                    //pictureBox1.Image = bmp;
                    //pictureBox1.Refresh();
                    height = bmp.Height;
                    width = bmp.Width;

                    DNA vDNA = new DNA();
                    vDNA.fileName = fileName;
                    vDNA.frameidx = num_frames;

                    r11 = 0; r12 = 0; r21 = 0; r22 = 0;
                    g11 = 0; g12 = 0; g21 = 0; g22 = 0;
                    b11 = 0; b12 = 0; b21 = 0; b22 = 0;
                    for (int i = 0; i < height - 1; i++)
                    {
                        for (int j = 0; j < width - 1; j++)
                        {
                            if (i < (height / 2) && j < (width / 2))
                            {
                                r11 += (int)bmp.GetPixel(j, i).R;
                                g11 += (int)bmp.GetPixel(j, i).G;
                                b11 += (int)bmp.GetPixel(j, i).B;
                            }
                            else if (i < (height / 2) && j >= (width / 2))
                            {
                                r12 += (int)bmp.GetPixel(j, i).R;
                                g12 += (int)bmp.GetPixel(j, i).G;
                                b12 += (int)bmp.GetPixel(j, i).B;
                            }
                            else if (i >= (height / 2) && j < (width / 2))
                            {
                                r21 += (int)bmp.GetPixel(j, i).R;
                                g21 += (int)bmp.GetPixel(j, i).G;
                                b21 += (int)bmp.GetPixel(j, i).B;
                            }
                            else
                            {
                                r22 += (int)bmp.GetPixel(j, i).R;
                                g22 += (int)bmp.GetPixel(j, i).G;
                                b22 += (int)bmp.GetPixel(j, i).B;
                            }
                        }
                    }
                    bmp = null;
                    hw = height * width / (float)4.0;
                    r11 /= hw; g11 /= hw; b11 /= hw;
                    r21 /= hw; g21 /= hw; b21 /= hw;
                    r12 /= hw; g12 /= hw; b12 /= hw;
                    r22 /= hw; g22 /= hw; b22 /= hw;

                    Debug.WriteLine(num_frames + ": " + r11 + "," + g11 + "," + b11 + " vs " + r12 + "," + g12 + "," + b12 + " vs " + r21 + "," + g21 + "," + b21 + " vs " + r22 + "," + g22 + "," + b22);

                    // byte레벨로 소수점 이하가 사라짐. 오차 발생 포인트
                    vDNA.r1 = (byte)r11; vDNA.r2 = (byte)r12; vDNA.r3 = (byte)r21; vDNA.r4 = (byte)r22;
                    vDNA.g1 = (byte)g11; vDNA.g2 = (byte)g12; vDNA.g3 = (byte)g21; vDNA.g4 = (byte)g22;
                    vDNA.b1 = (byte)b11; vDNA.b2 = (byte)b12; vDNA.b3 = (byte)b21; vDNA.b4 = (byte)b22;

                    dnas.Add(vDNA);
                }
                strFrameNumber = (++num_frames).ToString();
                if (num_frames > 10) break;
                filename = "c:\\RTVS\\Resources\\VideosUploaded\\snapshots\\ss-"
                + "00".Substring(0, 2 - strFrameNumber.Length) + strFrameNumber
                + ".jpg";

            }

            MAMDataContext db = new MAMDataContext();
            //2단계, 추출된 10개의 프레임중에서 DB와 비교해서 가장 가까운 프레임을 선정한다.
            foreach (DNA aDNA in dnas)
            {
                var tmp =   (from v in db.VDNA
                                select new
                                {
                                    sum_rgb = (Math.Abs(v.r1 - aDNA.r1) + Math.Abs(v.g1 - aDNA.g1) + Math.Abs(v.b1 - aDNA.b1)
                                            + Math.Abs(v.r2 - aDNA.r2) + Math.Abs(v.g2 - aDNA.g2) + Math.Abs(v.b2 - aDNA.b2)
                                            + Math.Abs(v.r3 - aDNA.r3) + Math.Abs(v.g3 - aDNA.g3) + Math.Abs(v.b3 - aDNA.b3)
                                            + Math.Abs(v.r4 - aDNA.r4) + Math.Abs(v.g4 - aDNA.g4) + Math.Abs(v.b4 - aDNA.b4))
                                }
                            ).OrderBy(i=>i.sum_rgb).FirstOrDefault();
                aDNA.sum_difference = tmp.sum_rgb;
                Debug.WriteLine("Difference: " + tmp.sum_rgb);
            }

            // 최소값 선택 완료
            DNA tdna = dnas.OrderBy(d => d.sum_difference).FirstOrDefault();

            // DNA Phase II: 연속적인 패턴값 추출
            for (int idx1 = tdna.frameidx+10; idx1 <= 5*10; idx1=idx1+10 )
            {
                strFrameNumber = idx1.ToString();
                filename = "c:\\RTVS\\Resources\\VideosUploaded\\snapshots\\ss-"
                            + "00".Substring(0, 2 - idx1.ToString().Length) + idx1.ToString() + ".jpg";
                if (!File.Exists(filename)) continue; 
                Debug.WriteLine("{0}: {1}", DateTime.Now, idx1);
                fileData = File.ReadAllBytes(filename);
                bmp = new Bitmap(new MemoryStream(fileData));
                //File.Delete(filename);

                if (bmp != null)
                {
                    height = bmp.Height;
                    width = bmp.Width;

                    DNA vDNA = new DNA();
                    vDNA.fileName = fileName;
                    vDNA.frameidx = num_frames;

                    r11 = 0; r12 = 0; r21 = 0; r22 = 0;
                    g11 = 0; g12 = 0; g21 = 0; g22 = 0;
                    b11 = 0; b12 = 0; b21 = 0; b22 = 0;
                    for (int i = 0; i < height - 1; i++)
                    {
                        for (int j = 0; j < width - 1; j++)
                        {
                            if (i < (height / 2) && j < (width / 2))
                            {
                                r11 += (int)bmp.GetPixel(j, i).R;
                                g11 += (int)bmp.GetPixel(j, i).G;
                                b11 += (int)bmp.GetPixel(j, i).B;
                            }
                            else if (i < (height / 2) && j >= (width / 2))
                            {
                                r12 += (int)bmp.GetPixel(j, i).R;
                                g12 += (int)bmp.GetPixel(j, i).G;
                                b12 += (int)bmp.GetPixel(j, i).B;
                            }
                            else if (i >= (height / 2) && j < (width / 2))
                            {
                                r21 += (int)bmp.GetPixel(j, i).R;
                                g21 += (int)bmp.GetPixel(j, i).G;
                                b21 += (int)bmp.GetPixel(j, i).B;
                            }
                            else
                            {
                                r22 += (int)bmp.GetPixel(j, i).R;
                                g22 += (int)bmp.GetPixel(j, i).G;
                                b22 += (int)bmp.GetPixel(j, i).B;
                            }
                        }
                    }
                    bmp = null;
                    hw = height * width / (float)4.0;
                    r11 /= hw; g11 /= hw; b11 /= hw;
                    r21 /= hw; g21 /= hw; b21 /= hw;
                    r12 /= hw; g12 /= hw; b12 /= hw;
                    r22 /= hw; g22 /= hw; b22 /= hw;

                    //snapshots[position] = bmp;
                    Debug.WriteLine(idx1 + ": " + r11 + "," + g11 + "," + b11 + " vs " + r12 + "," + g12 + "," + b12 + " vs " + r21 + "," + g21 + "," + b21 + " vs " + r22 + "," + g22 + "," + b22);

                    // byte레벨로 소수점 이하가 사라짐. 오차 발생 포인트
                    vDNA.r1 = (byte)r11; vDNA.r2 = (byte)r12; vDNA.r3 = (byte)r21; vDNA.r4 = (byte)r22;
                    vDNA.g1 = (byte)g11; vDNA.g2 = (byte)g12; vDNA.g3 = (byte)g21; vDNA.g4 = (byte)g22;
                    vDNA.b1 = (byte)b11; vDNA.b2 = (byte)b12; vDNA.b3 = (byte)b21; vDNA.b4 = (byte)b22;

                    int diff = 0;

                    //vDNAref = vDNAs.Where(v => v.idx == i).FirstOrDefault();
                    if (tdna != null)
                    {
                        diff = 0;
                        diff = Math.Abs(vDNA.r1 - tdna.r1);
                        diff += Math.Abs(vDNA.r2 - tdna.r2);
                        diff += Math.Abs(vDNA.r3 - tdna.r3);
                        diff += Math.Abs(vDNA.r4 - tdna.r4);
                        diff += Math.Abs(vDNA.g1 - tdna.g1);
                        diff += Math.Abs(vDNA.g2 - tdna.g2);
                        diff += Math.Abs(vDNA.g3 - tdna.g3);
                        diff += Math.Abs(vDNA.g4 - tdna.g4);
                        diff += Math.Abs(vDNA.b1 - tdna.b1);
                        diff += Math.Abs(vDNA.b2 - tdna.b2);
                        diff += Math.Abs(vDNA.b3 - tdna.b3);
                        diff += Math.Abs(vDNA.b4 - tdna.b4);

                        switch ((idx1-tdna.frameidx)/10)
                        {
                            case 1: tdna.diff1 = (short)diff;
                                break;
                            case 2: tdna.diff2 = (short)diff;
                                break;
                            case 3: tdna.diff3 = (short)diff;
                                break;
                            case 4: tdna.diff4 = (short)diff;
                                break;
                            //case 5: vDNAref.diff5 = diff;
                            //    break;
                            //case 6: vDNAref.diff6 = diff;
                            //    break;
                            //case 7: vDNAref.diff7 = diff;
                            //    break;
                            //case 8: vDNAref.diff8 = diff;
                            //    break;
                            //case 9: vDNAref.diff9 = diff;
                            //    break;
                        }
                    }
                }
            }

            var results = (from v in db.VDNA
                       select new
                       {
                           v.Videoid,
                           v.idx,
                           sum_rgb = (Math.Abs(v.r1 - tdna.r1) + Math.Abs(v.g1 - tdna.g1) + Math.Abs(v.b1 - tdna.b1)
                                   + Math.Abs(v.r2 - tdna.r2) + Math.Abs(v.g2 - tdna.g2) + Math.Abs(v.b2 - tdna.b2)
                                   + Math.Abs(v.r3 - tdna.r3) + Math.Abs(v.g3 - tdna.g3) + Math.Abs(v.b3 - tdna.b3)
                                   + Math.Abs(v.r4 - tdna.r4) + Math.Abs(v.g4 - tdna.g4) + Math.Abs(v.b4 - tdna.b4)
                                   + Math.Abs((int)v.diff1 - tdna.diff1) + Math.Abs((int)v.diff2 - tdna.diff2)
                                   + Math.Abs((int)v.diff3 - tdna.diff3) + Math.Abs((int)v.diff4 - tdna.diff4))
                       }
                ).OrderBy(i => i.sum_rgb).Take(10).ToList();

            string strResults = "";
            foreach (var avar in results)
            {
                //VSearchResult vSearchResult = new VSearchResult();
                strResults += avar.Videoid + ",";
                strResults += avar.idx + ",";
                strResults += avar.sum_rgb + "\r\n";
            }

            foreach (FileInfo f in new DirectoryInfo("c:\\RTVS\\Resources\\VideosUploaded\\snapshots\\").GetFiles("ss-*.jpg"))
            {
                f.Delete();
            }

            return strResults;
        }

    }
}
