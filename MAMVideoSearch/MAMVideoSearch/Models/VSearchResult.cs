﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MAMVideoSearch.Models
{
    public class VSearchResult
    {
        public string fileName { get; set; }
        public int frameidx { get; set; }
        public int videoid { get; set; }
        public int sum_difference { get; set; }
    }
}
