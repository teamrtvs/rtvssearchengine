﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MAMVideoSearch.Models
{
    public class DNA
    {
        public string fileName { get; set; }
        public int frameidx { get; set; }
        public byte r1 { get; set; }
        public byte g1 { get; set; }
        public byte b1 { get; set; }
        public byte r2 { get; set; }
        public byte g2 { get; set; }
        public byte b2 { get; set; }
        public byte r3 { get; set; }
        public byte g3 { get; set; }
        public byte b3 { get; set; }
        public byte r4 { get; set; }
        public byte g4 { get; set; }
        public byte b4 { get; set; }
        public Int16 diff1 { get; set; }
        public Int16 diff2 { get; set; }
        public Int16 diff3 { get; set; }
        public Int16 diff4 { get; set; }
        public int sum_difference { get; set; }
    }
}
